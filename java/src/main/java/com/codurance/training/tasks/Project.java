package com.codurance.training.tasks;

import java.util.ArrayList;
import java.util.List;


public class Project {
	
	public static ArrayList<Project> listProject = new ArrayList<Project>();
	
	private int id;
	private List<Task> tasks = new ArrayList<Task>();	
	
	public Project(int id, List<Task> tasks) {
		this.id = id;
		this.tasks = tasks;
		listProject.add(this);
	}
	
	public Project(int id) {
		this.id = id;
		listProject.add(this);
	}
	
	public static ArrayList<Project> getProjects(){
		return listProject;
	}

	public int getId() {
		return id;
	}
	
	public List<Task> getTasks(){
		return tasks;
	}
	
	public void addTask(Task task) {
		this.tasks.add(task);
	}
}
