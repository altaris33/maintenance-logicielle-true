package com.codurance.training.tasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Command {

	private static final String QUIT = "quit";

	private final BufferedReader in;
	private final PrintWriter out;

	public Command(BufferedReader in, PrintWriter out) {

		this.in = in;
		this.out = out;
	}

	public void show() {
		for (Project project : Project.getProjects()) {
			out.println(project.getId());
			for (Task task : project.getTasks()) {
				out.printf("    [%c] %d: %s%n", (task.isDone() ? 'x' : ' '), task.getId(), task.getDescription());
			}
			out.println();
		}
	}

	public void add(String commandLine) {
		String[] subcommandRest = commandLine.split(" ", 2);
		String subcommand = subcommandRest[0];
		if (subcommand.equals("project")) {
			addProject(subcommandRest[1]);
		} else if (subcommand.equals("task")) {
			String[] projectTask = subcommandRest[1].split(" ", 2);
			addTask(projectTask[0], projectTask[1]);
		}
	}

	public void addProject(String name) {
		Project project = new Project(3);
	}

	public void addTask(String project, String description) {
		List<Task> projectTasks = tasks.get(project);
		if (projectTasks == null) {
			out.printf("Could not find a project with the name \"%s\".", project);
			out.println();
			return;
		}
		projectTasks.add(new Task(nextId(), description, false));
	}

	public void check(String idString) {
		setDone(idString, true);
	}

	public void uncheck(String idString) {
		setDone(idString, false);
	}

	public void setDone(String idString, boolean done) {
		int id = Integer.parseInt(idString);
		for (Project project : Project.getProjects()) {
			for (Task task : project.getTasks()) {
				if (task.getId() == id) {
					task.setDone(done);
					return;
				}
			}
		}
		out.printf("Could not find a task with an ID of %d.", id);
		out.println();
	}

	public void help() {
		out.println("Commands:");
		out.println("  show");
		out.println("  add project <project name>");
		out.println("  add task <project name> <task description>");
		out.println("  check <task ID>");
		out.println("  uncheck <task ID>");
		out.println();
	}

	public void error(String command) {
		out.printf("I don't know what the command \"%s\" is.", command);
		out.println();
	}

}
