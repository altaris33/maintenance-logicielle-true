package com.codurance.training.tasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Runner implements Runnable {
    

    //private final Project project;
    private BufferedReader in;
    private PrintWriter out;
    
    private long lastId = 0;

    public Runner(BufferedReader in, PrintWriter out ) {
    	this.in = in;
    	this.out = out;
    }
    

    private long nextId() {
        return ++lastId;
    }
    
    private void execute(String commandLine) {
        String[] ARGS = commandLine.split(" ", 2);
        String commandName = ARGS[0];
        Command command = new Command(in, out);
        switch (commandName) {
            case "show":
                command.show();
                break;
            case "add":
                command.add(ARGS[1]);
                break;
            case "check":
                command.check(ARGS[1]);
                break;
            case "uncheck":
                command.uncheck(ARGS[1]);
                break;
            case "help":
                command.help();
                break;
            default:
                command.error(commandName);
                break;
        }
    }

	@Override
	public void run() {
		while (true) {
            out.print("> ");
            out.flush();
            String labelledCommand;
            try {
            	labelledCommand = in.readLine();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            if (labelledCommand.toLowerCase().equals("quit")) {
                break;
            }
            execute(labelledCommand);
        }
		
	}
}
