package com.codurance.training.tasks;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import org.junit.Test;

public class TaskTest {
	
	BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	PrintWriter out = new PrintWriter(System.out);
	Command command = new Command(in, out);
	
	@Test
    public void isTaskCreatedAndIdDisplayed() {
    	Task task = new Task(1, "manger bananes", true);
    	assertEquals(task.getId(), 1 );
    }
	
	@Test
    public void isTaskCreatedAndDescriptionDisplayed() {
    	Task task = new Task(1, "manger bananes", true);
    	assertEquals(task.getDescription(), "manger bananes" );
    }
	
	@Test
    public void isTaskCreatedAndDone() {
    	Task task = new Task(1, "manger bananes", true);
    	assertEquals(task.isDone(), true );
    }
	
	@Test
	public void isNewTaskAddedToProject() {
		Project project = new Project(1);
		Task taskToAdd = new Task(1, "manger fraises", true);
		project.addTask(taskToAdd);
		assertEquals(project.getTasks().get(0), taskToAdd);
	}
	
	@Test
	public void isTaskChecked() {
		Task task = new Task(1, "manger fraises", false);
		assertEquals(task.isDone(), false);
		command.check(String.valueOf(task.getId()));
		assertEquals(task.isDone(), true);
	}

}
